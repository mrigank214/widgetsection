package com.example.newswidget;



import com.example.newswidget.Model4.Headlines;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface1 {
    @GET("top-headlines")
    Call<Headlines> getHeadLines(
            @Query("country") String country,
            @Query("category") String category,
            @Query("apiKey") String apiKey

    );

    @GET("everything")
    Call<Headlines> getSpecificData(
            @Query("q") String query,
            @Query("apiKey") String apiKey);


}
