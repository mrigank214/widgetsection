package com.example.newswidget;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient1 {
    private static final String BASE_URL = "https://newsapi.org/v2/";
    private static ApiClient1 apiClient;
    private static Retrofit retrofit;

    private ApiClient1(){
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static synchronized ApiClient1 getInstance(){
        if (apiClient==null){
            apiClient = new ApiClient1();
        }
        return apiClient;
    }

    public ApiInterface1 getApi(){
        return retrofit.create(ApiInterface1.class);
    }
}
