package com.example.newswidget;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;

public class demofirebasetorecycler extends AppCompatActivity {


    RecyclerView recview;
    newadapter newadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demofirebasetorecycler);

        recview=(RecyclerView)findViewById(R.id.recview);
        recview.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<mymodel> options =
                new FirebaseRecyclerOptions.Builder<mymodel>()
                        .setQuery(FirebaseDatabase.getInstance().getReference().child("entertainment-feeds"), mymodel.class)
                        .build();


        newadapter=new newadapter(options);
        recview.setAdapter(newadapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        newadapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        newadapter.stopListening();
    }

}


