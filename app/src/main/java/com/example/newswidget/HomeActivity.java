package com.example.newswidget;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

public class HomeActivity extends AppCompatActivity {



    private Button b1,b2,b3,b4,b5,b6;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        b1=findViewById(R.id.buttonprofile);
        b2=findViewById(R.id.buttoneducation);
        b3=findViewById(R.id.buttonHealth);
        b4=findViewById(R.id.buttonGoal);
        b5=findViewById(R.id.buttonFinance);
        b6=findViewById(R.id.buttonComfort);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });
       b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
         b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity4();
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity5();
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity6();
            }
        });




    }


    public void openActivity1(){
        Intent intent=new Intent(this,MainActivity7.class);
        startActivity(intent);
    }

    public void openActivity2(){
        Intent intent=new Intent(this,MainActivity2.class);
        startActivity(intent);
    }

    public void openActivity3(){
        Intent intent=new Intent(this,MainActivity3.class);
        startActivity(intent);
    }

    public void openActivity4(){
        Intent intent=new Intent(this,MainActivity4.class);
        startActivity(intent);
    }

    public void openActivity5(){
        Intent intent=new Intent(this,MainActivity9.class);
        startActivity(intent);
    }

    public void openActivity6(){
        Intent intent=new Intent(this,MainActivity6.class);
        startActivity(intent);
    }




}
