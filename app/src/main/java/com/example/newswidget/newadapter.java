package com.example.newswidget;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class newadapter extends FirebaseRecyclerAdapter<mymodel,newadapter.myviewholder> implements View.OnClickListener {


    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public newadapter(@NonNull FirebaseRecyclerOptions<mymodel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myviewholder holder, int position, @NonNull mymodel model) {


            holder.name.setText(String.valueOf(model.getOrder()));
            holder.course.setText(model.getCreated_at());
            holder.email.setText(model.getType());
            Glide.with(holder.img.getContext()).load(model.getImage_url()).into(holder.img);

            // condition 1
            if (model.getOrder() == 1005) {
                //Glide.with(holder.img.getContext()).load(R.drawable.profile).into(holder.img);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), MainActivity7.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }
            // condition 2
            if (model.getOrder() == 2 || model.getOrder() == 6 || model.getOrder() == 1002) {
                Glide.with(holder.img.getContext()).load(R.drawable.vids).into(holder.img);
                holder.email.setText("Video Library");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), MainActivity2.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }
            // condition 3
            if (model.getOrder() == 8) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), BannerActivity2.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }
            // condition 4
            if (model.getOrder() == 4) {
               // Glide.with(holder.img.getContext()).load(R.drawable.scratch).into(holder.img);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(view.getContext(), MainActivity4.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }
            // condition 5
            if (model.getOrder() == 12) {
                Glide.with(holder.img.getContext()).load(R.drawable.wheelgo).into(holder.img);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), MainActivity3.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }
            // condition 6
            if (model.getOrder() == 11) {
               // Glide.with(holder.img.getContext()).load(R.drawable.birdgame).into(holder.img);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), MainActivity6.class);
                        view.getContext().startActivity(intent);
                    }
                });

            }


        }


    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.simplerow,parent,false);
        return new myviewholder(view);

    }

    @Override
    public void onClick(View v) {
        Intent intent =new Intent(v.getContext(),MainActivity7.class);
        v.getContext().startActivity(intent);
    }


    class myviewholder extends RecyclerView.ViewHolder {

        CardView cv;
        CircleImageView img;
        TextView name,course,email;
        public myviewholder(@NonNull View itemView) {
            super(itemView);

            img=(CircleImageView)itemView.findViewById(R.id.img1);
            name=(TextView)itemView.findViewById(R.id.nametext);
            course=(TextView)itemView.findViewById(R.id.coursetext);
            email=(TextView)itemView.findViewById(R.id.emailtext);


        }
    }

}
