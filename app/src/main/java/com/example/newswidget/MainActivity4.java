package com.example.newswidget;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.anupkumarpanwar.scratchview.ScratchView;

import java.util.Random;

public class MainActivity4 extends AppCompatActivity {

    TextView textView;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch);

        textView=findViewById(R.id.txtv);




        ScratchView scratchView=findViewById(R.id.scratchView);
        scratchView.setRevealListener(new ScratchView.IRevealListener() {
            @Override
            public void onRevealed(ScratchView scratchView) {
                Random rand=new Random();
                int number=rand.nextInt(100)+1;
                if(number==1)
                {   String num="You've Won\n\u20B9100";
                    textView.setText(num);

                    Toast.makeText(MainActivity4.this,num,Toast.LENGTH_SHORT).show();
                }
                else{
                    String num="Better Luck Next Time";
                    textView.setText(num);
                }
            }

            @Override
            public void onRevealPercentChangedListener(ScratchView scratchView, float percent) {

            }
        });
    }


}
